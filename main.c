#include "file_module.h"

int main(int argc, char *argv[]) {
    return (argc == 2) ? print_file_with_line_numbers(argv[1]) : -1;
}
