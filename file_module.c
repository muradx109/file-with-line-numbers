/// file_module.c

#include "file_module.h"

int print_file_with_line_numbers(const char *filename) {
    if (!filename) return -1;

    FILE *file = fopen(filename, "r");
    if (!file) return -1;

    char line[BUFSIZ]; /// BUFSIZ is a standard constant for buffer size
    int line_number = 1;

    while (fgets(line, sizeof(line), file)) {
        printf("%d: %s", line_number++, line);
    }

    fclose(file);
    return 0;
}
